#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2020 The LineageOS Project
# Copyright (C) 2024 The risingOS Android Project
#
# SPDX-License-Identifier: Apache-2.0
#

set -e

DEVICE=common
VENDOR=gms

# Load extract_utils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "${MY_DIR}" ]]; then MY_DIR="${PWD}"; fi

ANDROID_ROOT="${MY_DIR}/../.."

HELPER="${ANDROID_ROOT}/tools/extract-utils/extract_utils.sh"
if [ ! -f "${HELPER}" ]; then
    echo "Unable to find helper script at ${HELPER}"
    exit 1
fi
source "${HELPER}"

# Initialize the helper
setup_vendor "${DEVICE}" "${VENDOR}" "${ANDROID_ROOT}" true

# Warning headers and guards
write_headers "arm64"
sed -i 's|TARGET_DEVICE|TARGET_ARCH|g' "${ANDROIDMK}"
sed -i 's|vendor/gms/|vendor/gms/common|g' "${PRODUCTMK}"
sed -i 's|device/gms//setup-makefiles.sh|vendor/gms/setup-makefiles.sh|g' "${ANDROIDBP}" "${ANDROIDMK}" "${BOARDMK}" "${PRODUCTMK}"

{
    echo 'WITH_GMS_CORE ?= false'
    echo 'ifeq ($(strip $(WITH_GMS_CORE)),true)'
    echo 'PRODUCT_COPY_FILES += \'
    echo '    vendor/gms/common/proprietary/product/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.dialer.support.xml \'
    echo '    vendor/gms/common/proprietary/product/etc/permissions/privapp-permissions-google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-google.xml \'
    echo '    vendor/gms/common/proprietary/product/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google.xml \'
    echo '    vendor/gms/common/proprietary/product/etc/sysconfig/google-initial-package-stopped-states.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-initial-package-stopped-states.xml \'
    echo '    vendor/gms/common/proprietary/product/etc/sysconfig/google-staged-installer-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-staged-installer-whitelist.xml \'
    echo '    vendor/gms/common/proprietary/system/etc/permissions/privapp-permissions-google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-google.xml \'
    echo '    vendor/gms/common/proprietary/system_ext/etc/permissions/privapp-permissions-google-se.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-google-se.xml'
    echo ''
    echo 'PRODUCT_PACKAGES += \'
    echo '    GoogleServicesFramework \'
    echo '    Phonesky \'
    echo '    PrebuiltGmsCoreSc \'
    echo '    PrebuiltGmsCoreSc_AdsDynamite.uncompressed \'
    echo '    PrebuiltGmsCoreSc_DynamiteLoader.uncompressed \'
    echo '    PrebuiltGmsCoreSc_DynamiteModulesA \'
    echo '    PrebuiltGmsCoreSc_DynamiteModulesC \'
    echo '    PrebuiltGmsCoreSc_GoogleCertificates \'
    echo '    PrebuiltGmsCoreSc_MapsDynamite \'
    echo '    PrebuiltGmsCoreSc_MeasurementDynamite'
    echo ''
    echo 'else'
    echo ''
} >> "${MY_DIR}/${DEVICE}/common-vendor.mk"

write_makefiles "${MY_DIR}/proprietary-files.txt" true

# Finish
write_footers

# Overlays
echo -e "\ninclude vendor/gms/common/overlays.mk" >> "$PRODUCTMK"

# Gcam
sed -i '/GoogleCamera[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"

# Meet
sed -i '/name: "MeetPrebuilt_20240128"/,/product_specific: true,/s/product_specific: true,/&\n	skip_preprocessed_apk_checks: true,/' "${ANDROIDBP}"

{
    echo 'TARGET_PREBUILT_GOOGLE_CAMERA ?= false'
    echo 'ifeq ($(strip $(TARGET_PREBUILT_GOOGLE_CAMERA)),true)'
    echo 'PRODUCT_PACKAGES += \'
    echo '    GoogleCamera'
    echo 'endif'
    echo ""
} >> "${MY_DIR}/${DEVICE}/common-vendor.mk"

# Pixel Launcher
sed -i '/NexusLauncherRelease[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"

{
    echo 'TARGET_DEFAULT_PIXEL_LAUNCHER ?= false'
    echo 'ifeq ($(strip $(TARGET_DEFAULT_PIXEL_LAUNCHER)),true)'
    echo 'PRODUCT_PACKAGES += \'
    echo '    NexusLauncherRelease'
    echo 'endif'
    echo ""
} >> "${MY_DIR}/${DEVICE}/common-vendor.mk"

# HealthIntelligencePrebuilt
sed -i '/HealthIntelligencePrebuilt-1762[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"

{
    echo 'CURRENT_DEVICE := $(shell echo "$(TARGET_PRODUCT)" | cut -d'\''_'\'' -f 2,3)'
    echo 'ifneq ($(filter $(CURRENT_DEVICE), husky shiba akita),)'
    echo 'PRODUCT_PACKAGES += \'
    echo '    HealthIntelligencePrebuilt-1762'
    echo 'endif'
    echo ""
} >> "${MY_DIR}/${DEVICE}/common-vendor.mk"

# GMS Extras
sed -i '/Chrome-Stub[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/Drive[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/Maps[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/MeetPrebuilt_20240128[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/PrebuiltGmail[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/TrichromeLibrary-Stub[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/WebViewGoogle-Stub[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/AdaptiveVPNPrebuilt-10307[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/Photos[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/PixelSupportPrebuilt[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/RecorderPrebuilt_619037745[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/TipsPrebuilt_v5.2.0.595892729[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/Videos[[:space:]]*\\/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/vendor\/gms\/common\/proprietary\/product\/app\/Chrome\/Chrome.apk.gz/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/vendor\/gms\/common\/proprietary\/product\/app\/TrichromeLibrary\/TrichromeLibrary.apk.gz/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"
sed -i '/vendor\/gms\/common\/proprietary\/product\/app\/WebViewGoogle\/WebViewGoogle.apk.gz/d' "${MY_DIR}/${DEVICE}/common-vendor.mk"

{
    echo 'WITH_GMS_EXTRAS ?= false'
    echo 'ifeq ($(strip $(WITH_GMS_EXTRAS)),true)'
    echo 'PRODUCT_COPY_FILES += \'
    echo '    vendor/gms/common/proprietary/product/app/Chrome/Chrome.apk.gz:$(TARGET_COPY_OUT_PRODUCT)/app/Chrome/Chrome.apk.gz \'
    echo '    vendor/gms/common/proprietary/product/app/TrichromeLibrary/TrichromeLibrary.apk.gz:$(TARGET_COPY_OUT_PRODUCT)/app/TrichromeLibrary/TrichromeLibrary.apk.gz \'
    echo '    vendor/gms/common/proprietary/product/app/WebViewGoogle/WebViewGoogle.apk.gz:$(TARGET_COPY_OUT_PRODUCT)/app/WebViewGoogle/WebViewGoogle.apk.gz'
    echo ""
    echo 'PRODUCT_PACKAGES += \'
    echo '    Chrome-Stub \'
    echo '    Drive \'
    echo '    Maps \'
    echo '    MeetPrebuilt_20240128 \'
    echo '    PrebuiltGmail \'
    echo '    TrichromeLibrary-Stub \'
    echo '    WebViewGoogle-Stub \'
    echo '    AdaptiveVPNPrebuilt-10307 \'
    echo '    Photos \'
    echo '    PixelSupportPrebuilt \'
    echo '    RecorderPrebuilt_619037745 \'
    echo '    TipsPrebuilt_v5.2.0.595892729 \'
    echo '    Videos'
    echo 'endif'
    echo 'endif'
} >> "${MY_DIR}/${DEVICE}/common-vendor.mk"
